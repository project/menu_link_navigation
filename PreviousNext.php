<?php

namespace Drupal\menu_link_navigation;


use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityRepository;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Menu\MenuTreeParameters;
use GuzzleHttp\ClientInterface;

/**
 * Previous Next Navigation
 */
class PreviousNext {

  public function __construct() {

  }

  /**
   * Flatten a menulinktree object into a flat array.
   *
   * @param $tree
   * @param $links
   *
   * @return array
   */
  public function flattenTree($tree, &$links) {
    foreach ($tree as $branch) {
      // If there is a link then add it to the list.
      if (isset($branch->link)) {
        $links[] = [
          '#title' => $branch->link->getTitle(),
          '#type' => 'link',
          '#url' => $branch->link->getUrlObject(),
          '#weight' => $branch->link->getWeight(),
        ];
      }
      // Recurse the subtree and add those links as well.
      foreach ($branch->subtree as $child) {
        $links[] = [
          '#title' => $child->link->getTitle(),
          '#type' => 'link',
          '#url' => $child->link->getUrlObject(),
          '#weight' => $child->link->getWeight(),
        ];
        $this->flattenTree($child->subtree, $links);
      }
    }
    return $links;
  }

  /**
   * Build the previous and next navigation buttons.
   *
   * @param $preferred_menu
   * @param null $path
   *
   * @return array
   */
  public function getNavigation($preferred_menu, $path = NULL) {

    if (!$path) {
      $current_path = \Drupal::service('path.current')->getPath();
      $path = \Drupal::service('path.alias_manager')
        ->getAliasByPath($current_path);
    }

    $tree = $this->getMenuTree($preferred_menu);

    $links = [];
    $links = $this->flattenTree($tree, $links);

    while ($link = current($links)) {
      if ($link['#url']->toString() == $path) {
        if (!$previous = prev($links)) {
          reset($links);
        }
        else {
          next($links);
        }
        $next = next($links);
        break;
      }
      next($links);
    }

    $navigation = [];

    $navigation['menu_navigation'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['menu_navigation'],
      ],
    ];
    if (isset($previous) && $previous) {
      $navigation['menu_navigation']['previous'] = $previous;
      $navigation['menu_navigation']['previous']['#weight'] = 0;
      $navigation['menu_navigation']['previous']['#attributes']['class'] = [
        'menu_navigation__prev',
      ];

    }
    if (isset($next) && $next) {
      $navigation['menu_navigation']['next'] = $next;
      $navigation['menu_navigation']['next']['#weight'] = 1;
      $navigation['menu_navigation']['next']['#attributes']['class'] = [
        'menu_navigation__next',
      ];
    }

    if ((isset($next) && $next) || isset($previous) && $previous) {
      return $navigation;
    }
    return [];

  }

  /**
   * Retrieve a menu tree.
   *
   * @param string $preferred_menu
   *
   * @return \Drupal\Core\Menu\MenuLinkTreeElement[]
   */
  public function getMenuTree($preferred_menu = 'main') {
    // Get a blank tree.
    $menu_tree = \Drupal::menuTree();

    // Build the parameters array to be passed to the tree builder.
    $parameters = $menu_tree->getCurrentRouteMenuTreeParameters($preferred_menu);
    $parameters->expandedParents = [];

    $tree = $menu_tree->load($preferred_menu, $parameters);

    // Ensure the tree is sorted as per the menu UI.
    $manipulators = [
      ['callable' => 'menu.default_tree_manipulators:checkAccess'],
      ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
    ];
    $tree = $menu_tree->transform($tree, $manipulators);

    // Return the tree.
    return $tree;
  }
}